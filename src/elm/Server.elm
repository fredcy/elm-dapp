module Server exposing (..)

import Config
import Http
import Json.Decode as JD
import Json.Encode as JE


type alias SignatureState =
    { message : String
    , payload : String
    , signature : String -- signature received from wallet
    , publicKey : String -- public key used to sign
    , network : String -- of public key
    }


type alias HttpTag a msg =
    Result Http.Error a -> msg


type alias TokenInfo =
    { message : String
    }


requestTokenInfo : HttpTag TokenInfo msg -> Config.Config -> Cmd msg
requestTokenInfo tag { botHost, token } =
    let
        url =
            botHost ++ "/api/v1/token/" ++ token
    in
    Http.get { url = url, expect = Http.expectJson tag tokenInfoDecoder }


tokenInfoDecoder : JD.Decoder TokenInfo
tokenInfoDecoder =
    JD.map TokenInfo
        (JD.field "message" JD.string)


sendNotification : HttpTag (List String) msg -> Config.Config -> SignatureState -> Cmd msg
sendNotification tag { botHost, token } { message, signature, publicKey, network } =
    let
        body =
            JE.object
                [ ( "token", JE.string token )
                , ( "message", JE.string message )
                , ( "network", JE.string network )
                , ( "publicKey", JE.string publicKey )
                , ( "signature", JE.string signature )
                ]

        url =
            botHost ++ "/api/v1/register"
    in
    Http.post
        { url = url
        , body = Http.jsonBody body
        , expect = Http.expectJson tag notificationRespDecoder
        }


notificationRespDecoder : JD.Decoder (List String)
notificationRespDecoder =
    JD.field "messages" (JD.list JD.string)
