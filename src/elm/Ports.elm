port module Ports exposing (SignatureInfo, SignatureRequest, signatureRequest, signatureResponse)


type alias SignatureRequest =
    { network : String
    , payload : String
    }


type alias SignatureInfo =
    { signature : String
    , publicKey : String
    , network : String
    }


port signatureRequest : SignatureRequest -> Cmd msg


port signatureResponse : (SignatureInfo -> msg) -> Sub msg
