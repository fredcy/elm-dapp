module Crypto exposing (toPayload)

import Bytes.Encode
import Hex
import Hex.Convert


toPayload : String -> String
toPayload message =
    -- Convert message string to payload format needed for signing by Tezos wallets
    let
        prefix =
            "0501"

        lengthHex =
            String.length message |> Hex.toString |> String.padLeft 8 '0'

        messageHex =
            Bytes.Encode.string message |> Bytes.Encode.encode |> Hex.Convert.toString
    in
        prefix ++ lengthHex ++ messageHex

        
