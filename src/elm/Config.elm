module Config exposing (Config, Token, emptyConfig, flagDecoder)

import Json.Decode as JD


type alias Token =
    String


type alias Config =
    { botHost : String -- URL of the requesting bot
    , token : Token -- access token in the request from the bot
    , networks : List String -- all possible network choices
    }


emptyConfig =
    { botHost = "unknown"
    , token = "unknown"
    , networks = []
    }


flagDecoder : JD.Decoder Config
flagDecoder =
    JD.map3 Config
        (JD.field "botHost" JD.string)
        (JD.field "token" JD.string)
        (JD.field "networks" (JD.list JD.string))
