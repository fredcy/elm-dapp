module Main exposing (Model, init, main, update, view)

import Browser
import Browser.Navigation exposing (Key)
import Config
import Crypto
import Html exposing (..)
import Html.Attributes as HA
import Html.Events as HE
import Http
import Json.Decode as JD
import Json.Encode as JE
import Ports
import Server
import Utils


networkDefault =
    "delphinet"


type State
    = Start
    | HaveTokenInfo
        { message : String -- message to sign, from bot
        , payload : String -- raw payload to have the wallet sign, calculated from the message
        }
    | HaveSignature Server.SignatureState


type Msg
    = GotTokenInfo (Result Http.Error Server.TokenInfo)
    | SignClicked
    | SignatureReceived Ports.SignatureInfo
    | NetworkSelected String
    | ToggleHex
    | DevModeClicked
    | GotNotificationResponse (Result Http.Error (List String))
    | NotifyClicked


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Ports.signatureResponse SignatureReceived
        , view = view
        }


type alias Model =
    { state : State
    , config : Config.Config
    , errors : List String
    , network : String -- currently selected network
    , showHex : Bool
    , devMode : Bool
    }


defaultModel : Model
defaultModel =
    { state = Start
    , config = Config.emptyConfig
    , errors = []
    , network = networkDefault
    , showHex = False
    , devMode = False
    }


init : JD.Value -> ( Model, Cmd Msg )
init flags =
    case JD.decodeValue Config.flagDecoder flags of
        Ok config ->
            ( { defaultModel | config = config }
            , Server.requestTokenInfo GotTokenInfo config
            )

        Err error ->
            ( { defaultModel | errors = [ "Internal init error: " ++ JD.errorToString error ] }
            , Cmd.none
            )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case Debug.log "msg, state" ( msg, model.state ) of
        ( GotTokenInfo (Ok { message }), _ ) ->
            let
                state =
                    HaveTokenInfo { message = message, payload = Crypto.toPayload message }
            in
            ( { model | state = state }, Cmd.none )

        ( GotTokenInfo (Err error), _ ) ->
            let
                message =
                    "Unable to get token info: " ++ Utils.httpErrorToString error
            in
            ( { model | errors = message :: model.errors }, Cmd.none )

        ( SignClicked, HaveTokenInfo { payload } ) ->
            ( model, Ports.signatureRequest { payload = payload, network = model.network } )

        ( SignatureReceived { signature, publicKey, network }, HaveTokenInfo { message, payload } ) ->
            let
                signatureState =
                    { message = message
                    , payload = payload
                    , signature = signature
                    , publicKey = publicKey
                    , network = network
                    }

                modelNew =
                    { model | state = HaveSignature signatureState }
            in
            ( modelNew, Server.sendNotification GotNotificationResponse modelNew.config signatureState )

        ( NetworkSelected network, _ ) ->
            ( { model | network = network }, Cmd.none )

        ( ToggleHex, _ ) ->
            ( { model | showHex = not model.showHex }, Cmd.none )

        ( DevModeClicked, _ ) ->
            ( { model | devMode = not model.devMode }, Cmd.none )

        ( GotNotificationResponse (Ok messages), _ ) ->
            -- TODO
            ( model, Cmd.none )

        ( GotNotificationResponse (Err error), _ ) ->
            let
                message =
                    "Unable to send notification: " ++ Utils.httpErrorToString error
            in
            ( { model | errors = message :: model.errors }, Cmd.none )

        ( NotifyClicked, HaveSignature signatureState ) ->
            ( model, Server.sendNotification GotNotificationResponse model.config signatureState )

        ( _, state ) ->
            let
                _ =
                    Debug.log "ignoring msg in state" ( msg, state )
            in
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    div [ HA.class "app" ]
        [ viewHeader model
        , viewErrors model
        , viewMain model
        , viewFooter model
        , viewDebug model
        ]


viewHeader : Model -> Html msg
viewHeader model =
    div [ HA.class "header" ]
        [ h1 [] [ text "Elm dApp" ]
        ]


viewErrors : Model -> Html msg
viewErrors model =
    if List.length model.errors > 0 then
        let
            viewError error =
                li [] [ text error ]

            header =
                if List.length model.errors > 1 then
                    "Errors"

                else
                    "Error"
        in
        div [ HA.class "errors" ]
            [ h2 [] [ text "Error" ]
            , ul [] (List.map viewError model.errors)
            ]

    else
        text ""


viewMain : Model -> Html Msg
viewMain model =
    div [ HA.class "main" ]
        [ case model.state of
            HaveTokenInfo { message, payload } ->
                div [ HA.class "have-token-info" ]
                    [ h2 [] [ text "Message to sign" ]
                    , div [ HA.class "message" ] [ text message ]
                    , div [ HA.class "show-hex-container" ]
                        [ button [ HA.class "show-hex", HE.onClick ToggleHex ]
                            [ text
                                (if model.showHex then
                                    "hide hex payload"

                                 else
                                    "show hex payload"
                                )
                            ]
                        ]
                    , if model.showHex then
                        div [ HA.class "message-hex" ] [ text payload ]

                      else
                        text ""
                    , div [ HA.class "controls" ]
                        [ networkSelector model.network model.config.networks
                        , button [ HA.class "sign", HE.onClick SignClicked ] [ text "sign the message" ]
                        ]
                    ]

            HaveSignature sigInfo ->
                div [ HA.class "have-signature" ]
                    [ h2 [] [ text "Signature" ]
                    , div [] [ text sigInfo.signature ]
                    ]

            _ ->
                text ""
        ]


networkSelector : String -> List String -> Html Msg
networkSelector networkCurrent networks =
    let
        networkOption network =
            option [ HA.value network, HA.selected (network == networkCurrent) ] [ text network ]
    in
    select
        [ HA.class "network"
        , HE.onInput NetworkSelected
        ]
        (List.map networkOption networks)


viewFooter : Model -> Html Msg
viewFooter model =
    div [ HA.class "footer" ]
        [ text "Built with "
        , a [ HA.href "https://www.walletbeacon.io/" ] [ text "Beacon" ]
        , div [ HA.class "dev-button" ]
            [ button [ HA.class "dev-mode", HE.onClick DevModeClicked ] [ text "dev mode" ]
            ]
        ]


viewDebug : Model -> Html Msg
viewDebug model =
    if model.devMode then
        div [ HA.class "debug" ]
            [ button [ HE.onClick NotifyClicked ] [ text "notify" ]
            , div [ HA.class "model" ]
                [ h4 [] [ text "Model" ]
                , text (Debug.toString model)
                ]
            ]

    else
        text ""
