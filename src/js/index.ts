import { Elm } from '../elm/Main.elm'


const token = new URLSearchParams(window.location.search).get("token")

const flags = {
    "botHost": "http://localhost:4321",
    "token": token,
    "networks": Object.values(beacon.NetworkType),
}

const app = Elm.Main.init({
    node: document.getElementById('main'),
    flags: flags,
});


app.ports.signatureRequest.subscribe(async function(request) {
    // received signature request from Elm app

    //app.ports.signatureResponse.send({ signature: "edsigfakefakefake", publicKey: "edpkfakefakefake" }); return

    // get signature via Beacon
    const signatureInfo = await sign(request.network, request.payload);

    // send signature info back to Elm app
    app.ports.signatureResponse.send(signatureInfo)
});


async function sign(network, payload) {
    const client = new beacon.DAppClient({
        name: "Elm dApp",
        preferredNetwork: network,
    })
    
    // This should not be necessary, but without it the call to requestPermisions often hangs.
    await client.clearActiveAccount()

    const permissions = await client.requestPermissions({
        network: {
            type: network,
        }
    }) 

    const signature = await client.requestSignPayload({
        payload: payload,
    })

    return {
	signature: signature.signature,
	publicKey: permissions.accountInfo.publicKey,
	network: network,
    }
}
