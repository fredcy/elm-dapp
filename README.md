# Elm dApp

```
npm install
npm run dev
```

The dApp works in conjuction with an [application service](https://gitlab.com/fredcy/tzchatbot) that it expects to access at http://localhost:4321.


## Credits

Foundation built per https://blog.hercules-ci.com/elm/2018/11/21/using-elm-and-parcel-for-zero-configuration-web-asset-management/
